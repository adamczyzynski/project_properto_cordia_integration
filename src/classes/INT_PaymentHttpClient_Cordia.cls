/**
 * Created by adam on 26/09/18.
 */


@RestResource(UrlMapping = '/api/ax/payment/')
global class INT_PaymentHttpClient_Cordia {

    @HttpPost
    global static void newPayment() {
        String responseBody = '';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;


        String bodyString = EncodingUtil.base64Decode(EncodingUtil.base64Encode(RestContext.request.requestBody)).toString();
        //Fixed problem with apex string formatting;
        bodyString = bodyString.replace('\\', '').replace('"{', '{').replace('}"', '}');
        res.statusCode = 200;
        INT_PaymentDto_Cordia cordia;
        properto__Payment__c newPayment;
        try {
            String jsonString = bodyString;
            cordia = (INT_PaymentDto_Cordia) JSON.deserialize(bodyString, INT_PaymentDto_Cordia.class);
            System.debug('cordia: ' + cordia);

            newPayment = axPaymentToPropertoPayment(cordia);
            Database.insert(newPayment);
            res.statusCode = 200;
            System.debug('new payment in database: '+newPayment);
        } catch (INT_ValidationException intValidationException) {
            res.responseBody = Blob.valueOf(intValidationException.getMessage());
            System.debug('validation error');
            res.statusCode = 422;
        }

        catch (DmlException dmlException) {
            System.debug('Dml exception ' + dmlException.getMessage());
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('DML exception:' + dmlException.getMessage());
        } catch (Exception exc) {
            System.debug('exception: ' + exc.getMessage());
            res.statusCode = 513;
            res.responseBody = Blob.valueOf('Problem :' + exc.getMessage());
        } finally {

            res.addHeader('Content-Type', 'application/json;charset=UTF-8');
            res.addHeader('Content-Type', 'text/plain');
        }
        if (cordia == null) {
            System.debug('Deserialisation error');
            res.statusCode = 512;
            res.responseBody = Blob.valueOf('Deserialisation error');
        } else {

            res.responseBody = Blob.valueOf('new payment correct: ' + newPayment);
        }

    }

    private static properto__Payment__c axPaymentToPropertoPayment(INT_PaymentDto_Cordia intPaymentDtoCordia) {
        System.debug('zaczynam');
        properto__Payment__c payment = INT_PaymentDto_Cordia.paymentCordiaToPayment(intPaymentDtoCordia);
        System.debug('wygenerowany payment: ' + payment);
        return payment;

    }


}