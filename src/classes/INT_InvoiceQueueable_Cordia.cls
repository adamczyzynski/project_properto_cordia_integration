/**
 * Created by adam on 22/10/18.
 */

public without sharing class INT_InvoiceQueueable_Cordia implements Queueable, Database.AllowsCallouts {


    List<properto__Invoice__c> invoicesToSend;

    public INT_InvoiceQueueable_Cordia(List<properto__Invoice__c> invoices) {
        this.invoicesToSend = invoices;

    }

    public void execute(QueueableContext context) {
        INT_Invoice_Service_Cordia intInvoiceServiceCordia = new INT_Invoice_Service_Cordia(invoicesToSend);
        intInvoiceServiceCordia.sendInvoices();

    }
}