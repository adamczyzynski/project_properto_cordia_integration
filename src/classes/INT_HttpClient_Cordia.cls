/**
 * Created by adam on 12/10/18.
 */

public without sharing class INT_HttpClient_Cordia {

    private static final String endpoint = 'http://54.38.136.127:8081';
    private static final String auth = properto__ESBhost__c.getValues('prod').ESBUserAndPassword__c;

    private HttpRequest httpRequest;

    public INT_HttpClient_Cordia(String path, String method, String jsonMessage) {
        this.httpRequest = new HttpRequest();

        this.httpRequest.setBody(jsonMessage);

        this.httpRequest.setEndpoint(endpoint + path);

        this.httpRequest.setMethod(method);

    }

    public void setHeader(String headerKey, String headerValue) {
        this.httpRequest.setHeader(headerKey, headerValue);
    }

    public HttpResponse send() {
        if (!Test.isRunningTest()) {
            Http http = new Http();
            this.httpRequest.setTimeout(10000);
            this.httpRequest.setHeader('Content-Type', 'application/json;charset=UTF-8');
            System.debug('Cordia http body json: ' + this.httpRequest.getBody());
            Blob authBlob = Blob.valueOf(auth);
            String basicAuth = EncodingUtil.base64Encode(authBlob);
            httpRequest.setHeader('Authorization', 'Basic ' + basicAuth);
            System.debug('http req test: ' + httpRequest);

            return http.send(this.httpRequest);
        }
        else{
            HttpResponse httpResponse = new HttpResponse();
            httpResponse.setStatusCode(204);
            httpResponse.setStatus('ok');
            httpResponse.setHeader('Content-Type', 'application/json;charset=UTF-8');
            return httpResponse;
        }
    }
}