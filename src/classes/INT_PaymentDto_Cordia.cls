/**
 * Created by adam on 26/09/18.
 */

global class INT_PaymentDto_Cordia {

    public String company { get; set; }

    public String bankAccount { get; set; }

    public String statementNo { get; set; }

    public Date transDate { get; set; }

    public String description { get; set; }

    public String currencyCode { get; set; }

    public Decimal amount { get; set; }

    public String senderBankAccount { get; set; }

    public String salesAgreementId { get; set; }

    public String customerId { get; set; }

    public String paymentType { get; set; }


    public static properto__Payment__c paymentCordiaToPayment(INT_PaymentDto_Cordia intPaymentDtoCordia) {
        properto__Payment__c payment = new properto__Payment__c();
        Boolean isIncomingPaymentInstallment;
        if (intPaymentDtoCordia.amount != null) {
            isIncomingPaymentInstallment = intPaymentDtoCordia.amount > 0.0;
        }
        Id salesforceOfferAgreementId = INT_CordiaHelper.cordiaAgreementIdToSalesProcessId(intPaymentDtoCordia.salesAgreementId);
        Id salesforceAgreementIdForPayment;
        if (isIncomingPaymentInstallment) {
            System.debug('przychodzacy');
            payment.RecordTypeId = properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_PAYMENT, properto.CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT);
            salesforceAgreementIdForPayment = [
                    SELECT Id
                    FROM properto__Sales_Process__c
                    WHERE properto__Offer__c = :salesforceOfferAgreementId
                    AND RecordType.DeveloperName = :properto.CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL
            ].Id;
        } else {
            payment.RecordTypeId = properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_PAYMENT, properto.CommonUtility.PAYMENT_TYPE_REIMBURSEMENT);

            salesforceAgreementIdForPayment = salesforceOfferAgreementId;
            System.debug('zwrot');
        }
        System.debug('sf agreement id: ' + salesforceAgreementIdForPayment);
        System.debug('przed record type');
        //RecordType recordTypeIncomingPayment = [SELECT Id, DeveloperName, Name FROM RecordType WHERE DeveloperName = :CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT];
        payment.RecordTypeId = properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_PAYMENT, properto.CommonUtility.PAYMENT_TYPE_INCOMING_PAYMENT);
        payment.properto__Payment_Type__c = 'Trust account payment';
        payment.properto__Has_Invoice__c = false;
        System.debug('przed ifami');
        if (intPaymentDtoCordia.statementNo != null) {
            payment.properto__Statement_Number__c = intPaymentDtoCordia.statementNo;
        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: Statement_Number__c');
        }
        if (intPaymentDtoCordia.transDate != null) {
            payment.properto__Payment_date__c = intPaymentDtoCordia.transDate;
            payment.properto__Statement_Date__c = Date.valueOf(intPaymentDtoCordia.transDate);
        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: Payment_date__c');
        }
        if (intPaymentDtoCordia.bankAccount != null) {
            payment.properto__Bank_Account_For_Resource__c = intPaymentDtoCordia.bankAccount;
        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: Bank_Account_For_Resource__c');
        }
        if (intPaymentDtoCordia.description != null) {
            payment.properto__Transfer_description__c = intPaymentDtoCordia.description;
        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: Transfer_description__c');
        }
        if (intPaymentDtoCordia.currencyCode != null) {
            payment.CurrencyIsoCode = intPaymentDtoCordia.currencyCode;
            payment.properto__Statement_Currency__c = intPaymentDtoCordia.currencyCode;
            payment.properto__Incoming_Payment_Currency__c = intPaymentDtoCordia.currencyCode;
        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: currencyCode');
        }
        if (intPaymentDtoCordia.salesAgreementId != null) {
            if (isIncomingPaymentInstallment) {
                payment.properto__After_sales_from_Incoming_Payment__c = salesforceAgreementIdForPayment;
            } else {
                payment.properto__Sale_Terms_Reimbursement__c = INT_CordiaHelper.cordiaAgreementIdToSalesProcessId(intPaymentDtoCordia.salesAgreementId);
                payment.properto__Agreements_Installment__c = INT_CordiaHelper.cordiaAgreementIdToSalesProcessId(intPaymentDtoCordia.salesAgreementId);
            }


        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: salesAgreementId');
        }
        if (intPaymentDtoCordia.amount != null) {
            if (isIncomingPaymentInstallment) {
                payment.properto__Paid_Value__c = intPaymentDtoCordia.amount;
                payment.properto__Paid_value_from_statement__c = intPaymentDtoCordia.amount;
            } else {
                payment.properto__Amount_to_pay__c = -intPaymentDtoCordia.amount;
            }

        } else {
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: amount');

        }

        if (intPaymentDtoCordia.paymentType != null && isIncomingPaymentInstallment) {
            payment.properto__Payment_Type__c = getPropertoPaymentTypeBasedOnCordiaPaymentType(intPaymentDtoCordia.paymentType);
        }
/*        if (intPaymentDtoCordia.senderBankAccount != null) {
            payment.Bank_Account_For_Resource__c = intPaymentDtoCordia.senderBankAccount;
        } else {
            System.debug('wywalilem sie');
            throw new INT_ValidationException('INT_PaymentDto_Cordia: Field is null: senderBankAccount');
        }*/
        System.debug('po logach, salesProcessID: '+intPaymentDtoCordia.salesAgreementId);

        return payment;
    }
    private static String getPropertoPaymentTypeBasedOnCordiaPaymentType(String cordiaPaymentType) {
        return getPaymentTypeIncomingMap().get(cordiaPaymentType);
    }

    private static Map<String, String> getPaymentTypeIncomingMap() {
        Map<String, String> paymentMap = new Map<String, String>();
        //AxPaymentType, PaymentType
        paymentMap.put('Release', 'Release');
        paymentMap.put('Payment', 'Trust account payment');
        return paymentMap;
    }
}