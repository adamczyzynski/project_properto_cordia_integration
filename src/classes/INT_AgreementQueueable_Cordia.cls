/**
 * Created by adam on 16/10/18.
 */

public without sharing class INT_AgreementQueueable_Cordia implements Queueable, Database.AllowsCallouts {


    List<properto__Sales_Process__c> salesProcessesToSend;
    List<CustomerAgreement> agreementsCustomers;
    Map<Id, properto__Sales_Process__c> idSalesProcessMap;
    Set<Id> notSyncedSalesProcessIds;


    public INT_AgreementQueueable_Cordia(List<properto__Sales_Process__c> salesProcesses) {

        this.salesProcessesToSend = [
                SELECT Id, Name, isSynchronizedWithAx__c, RecordType.DeveloperName, RecordTypeId, properto__Offer__c
                FROM properto__Sales_Process__c
                WHERE Id IN :salesProcesses
        ];
        this.idSalesProcessMap = new Map<Id, properto__Sales_Process__c>();
        this.notSyncedSalesProcessIds = new Set<Id>();
        for (properto__Sales_Process__c process : salesProcessesToSend) {
            idSalesProcessMap.put(process.Id, process);
        }

    }

    public void execute(QueueableContext context) {
        agreementsCustomers = new List<INT_AgreementQueueable_Cordia.CustomerAgreement>();
        for (properto__Sales_Process__c process : this.salesProcessesToSend) {
            agreementsCustomers.add(new CustomerAgreement(process));
        }
        Set<properto__Sales_Process__c> salesProcessesToUpdateSyncStatus = new Set<properto__Sales_Process__c>();

        for (CustomerAgreement customerAgreement : agreementsCustomers) {
            properto__Sales_Process__c returnedSalesProcess;
            returnedSalesProcess = customerAgreement.sendNewCustomerWithAgreements();
            if (returnedSalesProcess != null) {
                properto__Sales_Process__c process = idSalesProcessMap.get(returnedSalesProcess.id);
                process.isSynchronizedWithAx__c = false;
                salesProcessesToUpdateSyncStatus.add(process);
            }
        }

        if (salesProcessesToUpdateSyncStatus.size() > 0) {
            Database.update(new List<properto__Sales_Process__c>(salesProcessesToUpdateSyncStatus));
        }

    }

    public class CustomerAgreement {

        properto__Sales_Process__c process;

        public List<INT_CustomerDto_Cordia> customerDtoCordiaList;

        public INT_AgreementDto_Cordia agreementDtoCordia;

        public Boolean customerSendSuccessful;

        public Boolean agreementSendSuccessful;


        public customerAgreement(properto__Sales_Process__c process) {
            this.process = process;


            //Dodac wszystki klientow z procesu
            this.customerDtoCordiaList = INT_CustomerDto_Cordia.getAllContactsFromSalesProcess(process);
            this.agreementDtoCordia = new INT_AgreementDto_Cordia(process);
        }

        public properto__Sales_Process__c sendNewCustomerWithAgreements() {
            HttpResponse httpResponse = trySend();
            System.debug('http status code: ' + httpResponse.getStatusCode());
            if (httpResponse.getStatusCode() == 200 || httpResponse.getStatusCode() == 204) {
                System.debug('Successful new Sales Process: ' + process.id);
                return null;
            } else {
                System.debug('Failed new Sales Process: ' + process.id);
                return process;
            }
        }

        private HttpResponse trySend() {

            List<INT_CustomerDto_Cordia> customerDtoCordiaToResend = new List<INT_CustomerDto_Cordia>();

            for (INT_CustomerDto_Cordia customerDtoCordia : this.customerDtoCordiaList) {

                HttpResponse httpResponse = INT_CustomerHttpClient_Cordia.sendCustomer(customerDtoCordia);
            }

            INT_AgreementHttpService agreementHttpService = new INT_AgreementHttpService(this.agreementDtoCordia);

            HttpResponse httpResponse = agreementHttpService.sendNewAgreement();

            return httpResponse;

        }
    }
}