/**
 * Created by adam on 17/08/18.
 */


global class INT_CustomerHttpClient_Cordia {
    final static String path = '/customers';


    public static void sendUpdateExistAccount(Account account){
        String jsonCustomer = INT_CustomerHttpService.generateJsonCustomerDto(account);

        INT_HttpClient_Cordia cordiaHttpClient = new INT_HttpClient_Cordia(path, 'PUT', jsonCustomer);
        cordiaHttpClient.setHeader('Content-Type', 'application/json');
        HttpResponse httpResponse = cordiaHttpClient.send();
        System.debug(httpResponse.getBody());
    }

    public static HttpResponse sendCustomerFromSalesProcess(properto__Sales_Process__c process){

        INT_CustomerDto_Cordia customerDtoCordia = INT_CustomerDto_Cordia.customerDtoCordiaFromSalesProcess(process, null);
        String jsonToSend = JSON.serialize(customerDtoCordia);
        INT_HttpClient_Cordia httpClientCordia = new INT_HttpClient_Cordia(path, 'POST', jsonToSend);
        httpClientCordia.setHeader('Content-Type', 'application/json');
        return httpClientCordia.send();

    }

    public static HttpResponse sendCustomer(INT_CustomerDto_Cordia customerDtoCordia){

        String jsonToSend = JSON.serialize(customerDtoCordia);
        INT_HttpClient_Cordia httpClientCordia = new INT_HttpClient_Cordia(path, 'POST', jsonToSend);
        httpClientCordia.setHeader('Content-Type', 'application/json');
        return httpClientCordia.send();

    }
}