/**
 * Created by adam on 29/11/18.
 */

public without sharing class TH_SalesProcessTrigger extends properto.TriggerHandler.DelegateBase {


    public Map<Id, Id> spToLineMapForUpdating5VatApplicableField;

    public Set<properto__Sales_Process__c> salesProcessesToSend;
    public override void prepareBefore() {
        this.salesProcessesToSend = new Set<properto__Sales_Process__c>();
    }

    public override void prepareAfter() {

        spToLineMapForUpdating5VatApplicableField = new Map<Id, Id>();

        this.salesProcessesToSend = new Set<properto__Sales_Process__c>();
    }


    public override void beforeInsert(List<sObject> o) {
        /*if(TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            List<properto__Sales_Process__c> newSalesProcessList = (List<properto__Sales_Process__c>)o;
            for(properto__Sales_Process__c spN : newSalesProcessList) {

                

            }
        }*/
    }


    public override void beforeUpdate(Map<Id, sObject> old, Map<Id, sObject> o) {

        /*if(TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Sales_Process__c> oldSalesProcessMap = (Map<Id, properto__Sales_Process__c>)old;
            Map<Id, properto__Sales_Process__c> newSalesProcessMap = (Map<Id, properto__Sales_Process__c>)o;
            for(Id key : newSalesProcessMap.keySet()) {
                properto__Sales_Process__c spN = newSalesProcessMap.get(key);
                properto__Sales_Process__c spO = oldSalesProcessMap.get(key);

               
                  
            }
        }*/
        if (TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Sales_Process__c> oldSalesProcessMap = (Map<Id, properto__Sales_Process__c>) old;
            Map<Id, properto__Sales_Process__c> newSalesProcessMap = (Map<Id, properto__Sales_Process__c>) o;

            for (Id key : newSalesProcessMap.keySet()) {
                properto__Sales_Process__c spN = newSalesProcessMap.get(key);
                properto__Sales_Process__c spO = oldSalesProcessMap.get(key);
                //final
                if (canBeUsedToSyncWithAx(spO, spN)) {
                    if (spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                            properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)
                            && spN.properto__Status__c != properto.CommonUtility.SALES_PROCESS_STATUS_CANCELLATION_COPY
                            && spN.properto__Date_of_signing__c != null && spN.properto__Full_value_of_the_contract_paid__c) {
                        System.debug('!nowy SP w final ' + spN.Name);
                        spN.isSynchronizedWithAx__c = true;
                        salesProcessesToSend.add(spN);
                    }
                    //Adam Czyzysnki
                    // cordia integration

                    //Reservation
                    if (spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                            properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT)
                            && spN.properto__Status__c != properto.CommonUtility.SALES_PROCESS_STATUS_CANCELLATION_COPY
                            && spN.properto__Date_of_signing_Reservation__c != null) {
                        System.debug('!nowy SP w preliminary ' + spN.Name);
                        spN.isSynchronizedWithAx__c = true;

                        salesProcessesToSend.add(spN);
                    }
                    //Handover
                    if (spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                            properto.CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
                            && spN.properto__Status__c != properto.CommonUtility.SALES_PROCESS_STATUS_CANCELLATION_COPY
                            && spN.properto__Date_of_signing__c != null && spN.properto__Repertory_Number__c != null) {
                        System.debug('!nowy SP w handover ' + spN.Name);
                        salesProcessesToSend.add(spN);
                        spN.isSynchronizedWithAx__c = true;

                    }

                }

            }
        }
    }

    public override void afterInsert(Map<Id, sObject> o) {
        System.debug('test in th sales process');
        if (properto.TriggerHelper.userProfile == true || properto.TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Sales_Process__c> newSalesProcessMap = (Map<Id, properto__Sales_Process__c>) o;

            for (Id keyNew : newSalesProcessMap.keySet()) {

                properto__Sales_Process__c spN = newSalesProcessMap.get(keyNew);

/*
                if (spN.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL
                        && spN.properto__Status__c != properto.CommonUtility.SALES_PROCESS_STATUS_CANCELLATION_COPY) {
                    afterSalesServiceSalesProcessesToSend.add(spN);

                } else if (spN.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_AGREEMENT_TYPE_PRELIMINARY
                        && spN.properto__Status__c != properto.CommonUtility.SALES_PROCESS_STATUS_CANCELLATION_COPY && spN.properto__Date_of_signing__c != null) {
                    reservationAgreementSalesProcessesToSend.add(spN);
                } else if (spN.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT && spN.properto__Date_of_signing__c != null) {
                    finalAgreementsSalesProcessesToSend.add(spN);
                }
*/


                //Mateusz Wolak-Ksiazek
                //update X5_VAT_applicable__c field
                if (
                        spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS, properto.CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
                                && spN.properto__Product_Line_to_Proposal__c != null
                                && spN.properto__Investment_country__c == properto.CommonUtility.ROMANIA
                        ) {
                    spToLineMapForUpdating5VatApplicableField.put(spN.properto__Product_Line_to_Proposal__c, spN.Id);
                }

            }
        }


    }

    public override void afterUpdate(Map<Id, sObject>old, Map<Id, sObject> o) {
        if (TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Sales_Process__c> oldSalesProcessMap = (Map<Id, properto__Sales_Process__c>) old;
            Map<Id, properto__Sales_Process__c> newSalesProcessMap = (Map<Id, properto__Sales_Process__c>) o;

            for (Id key : newSalesProcessMap.keySet()) {
                properto__Sales_Process__c spN = newSalesProcessMap.get(key);
                properto__Sales_Process__c spO = oldSalesProcessMap.get(key);


                //Mateusz Wolak-Ksiazek
                //update X5_VAT_applicable__c field
                if (
                        spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS, properto.CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
                                && spN.properto__Offer_Line_to_Sale_Term__c != null
                                && spO.properto__Offer_Line_to_Sale_Term__c != spN.properto__Offer_Line_to_Sale_Term__c
                                && spN.properto__Investment_country__c == properto.CommonUtility.ROMANIA
                        ) {
                    spToLineMapForUpdating5VatApplicableField.put(spN.properto__Offer_Line_to_Sale_Term__c, spN.Id);
                }

                //Mateusz Wolak-Ksiazek
                //update X5_VAT_applicable__c field
                if (
                        spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS, properto.CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
                                && spN.properto__Offer_Line_to_After_sales_Service__c != null
                                && spO.properto__Offer_Line_to_After_sales_Service__c != spN.properto__Offer_Line_to_After_sales_Service__c
                                && spN.properto__Investment_country__c == properto.CommonUtility.ROMANIA
                        ) {
                    spToLineMapForUpdating5VatApplicableField.put(spN.properto__Offer_Line_to_After_sales_Service__c, spN.Id);
                }

                //Mateusz Wolak-Ksiazek
                //update X5_VAT_applicable__c field
                if (
                        spN.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS, properto.CommonUtility.SALES_PROCESS_TYPE_OFFER_LINE)
                                && spN.properto__Net_Price_With_Discount__c != null
                                && spO.properto__Net_Price_With_Discount__c != spN.properto__Net_Price_With_Discount__c
                                && spN.properto__Investment_country__c == properto.CommonUtility.ROMANIA
                        ) {
                    spToLineMapForUpdating5VatApplicableField.put(spN.properto__Offer_Line_to_After_sales_Service__c, spN.Id);
                }

            }
        }
    }


    public override void beforeDelete(Map<Id, sObject> old) {
        /*if(TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Sales_Process__c> salesProcessOldMap = (Map<Id, properto__Sales_Process__c>)old;
            for(Id key : salesProcessOldMap.keySet()) {
                properto__Sales_Process__c spO = salesProcessOldMap.get(key);
            }
        }*/
    }


    public override void afterDelete(Map<Id, sObject> o) {
        /* if(TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
             Map<Id, properto__Sales_Process__c> oldSalesProcessMap = (Map<Id, properto__Sales_Process__c>)o;

             for(Id key : oldSalesProcessMap.keySet()) {

                 properto__Sales_Process__c spO = oldSalesProcessMap.get(key);

             }
         }*/
    }

    @TestVisible
    private static Boolean canBeUsedToSyncWithAx(properto__Sales_Process__c newSalesProcess, properto__Sales_Process__c oldSalesProcess) {
        return !(newSalesProcess.isSynchronizedWithAx__c || oldSalesProcess.isSynchronizedWithAx__c);
    }


    public override void finish() {

        //Mateusz Wolak-Ksiazek
        //update X5_VAT_applicable__c field 
        if (spToLineMapForUpdating5VatApplicableField != null && !spToLineMapForUpdating5VatApplicableField.isEmpty()) {
            SalesProcessManager_Cordia.calculate5VatApplicable(spToLineMapForUpdating5VatApplicableField, null);
        }

        /** Cordia integration */

/*        if (this.salesProcessesToSend != null && this.salesProcessesToSend.size() > 0
                && properto__IntegrationServices__c.getInstance('Mule').properto__IsServiceAvailable__c == true) {

            List<properto__Sales_Process__c> salesProcesses = new List<properto__Sales_Process__c>();
            salesProcesses.addAll(this.salesProcessesToSend);
            System.enqueueJob(new INT_AgreementQueueable_Cordia(salesProcesses));
        }*/
    }
}