/**
 * Created by adam on 04/01/19.
 */

@IsTest
private class INT_Invoice_CordiaTest {
    static testMethod void testBehavior() {

    }

    @IsTest
    public static void shouldReturnCorrectInvoiceAdvanceType() {
        properto__Invoice__c invoice = properto.TestHelper.createInvoiceAdvance(null, false);
        insert invoice;

        invoice = [SELECT Id, RecordType.DeveloperName FROM properto__Invoice__c WHERE Id = :invoice.Id];
        System.assertEquals(INT_CordiaHelper.INVOICE_TYPE_ADVANCE, INT_Invoice_Cordia.setInvoiceType(invoice));
    }


}