/**
 * Created by adam on 02/10/18.
 */

global class INT_AgreementHttpService {

    private properto__Sales_Process__c salesProcess;

    private INT_AgreementDto_Cordia agreementDtoCordia;

    private List<INT_AgreementDto_Cordia> agreementDtoCordias;

    private List<properto__Sales_Process__c> salesProcesses;

    private String jsonToSend;

    private static final String path = '/agreement';

    final static String endpoint = 'http://54.38.136.127:8081/agreement';

    public INT_AgreementHttpService(properto__Sales_Process__c process) {
        this.salesProcess = process;
        this.agreementDtoCordia = new INT_AgreementDto_Cordia(process);
    }

    public INT_AgreementHttpService(List<properto__Sales_Process__c> salesProcesses) {
        this.salesProcesses = salesProcesses;
        this.agreementDtoCordias = new List<INT_AgreementDto_Cordia>();
        System.debug(salesProcesses.size());
        for (properto__Sales_Process__c salesProcess : salesProcesses) {
            this.agreementDtoCordias.add(new INT_AgreementDto_Cordia(salesProcess));
        }
    }

    public INT_AgreementHttpService(INT_AgreementDto_Cordia agreementDtoCordiaToSend){
        this.agreementDtoCordia = agreementDtoCordiaToSend;
    }

    public INT_AgreementHttpService(List<INT_AgreementDto_Cordia> agreementDtoCordiasToSend){
        this.agreementDtoCordias = agreementDtoCordiasToSend;
    }


    public HttpResponse sendNewAgreement() {
        HttpResponse httpResponse;
        if (!Test.isRunningTest()) {
            if (agreementDtoCordias == null) {

                this.jsonToSend = JSON.serialize(this.agreementDtoCordia);
                INT_HttpClient_Cordia httpClientCordia = new INT_HttpClient_Cordia(path, 'POST', jsonToSend);
                System.debug('Agreement to send: '+jsonToSend);
                httpClientCordia.setHeader('Content-Type', 'application/json');
                httpResponse = httpClientCordia.send();
                System.debug('response: ' + httpResponse);
            } else {
                System.debug(agreementDtoCordias.size());
                for (INT_AgreementDto_Cordia agreementCordia : agreementDtoCordias) {
                    System.debug('przed serializacja: ' + agreementCordia);
                    String jsonToSend = JSON.serialize(agreementCordia);
                    System.debug('json: ' + jsonToSend);
                    INT_HttpClient_Cordia httpClientCordia = new INT_HttpClient_Cordia(path, 'POST', jsonToSend);
                    httpClientCordia.setHeader('Content-Type', 'application/json');
                    if (!Test.isRunningTest()) {
                        httpResponse = httpClientCordia.send();
                        System.debug('response: ' + httpResponse.getBody());
                    }
                }

            }

        }
        return httpResponse;
    }

}