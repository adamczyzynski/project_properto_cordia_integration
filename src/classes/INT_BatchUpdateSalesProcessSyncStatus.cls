/**
 * Created by adam on 08/01/19.
 */

global class INT_BatchUpdateSalesProcessSyncStatus implements Database.Batchable<sObject>, Database.Stateful {

    private List<properto__Sales_Process__c> salesProcessesToUpdate = new List<properto__Sales_Process__c>();

    private Set<Id> salesProcessesIds = new Set<Id>();

    public INT_BatchUpdateSalesProcessSyncStatus(Set<Id> idsSalesProcess) {
        this.salesProcessesIds = idsSalesProcess;
    }

    global Iterable<SObject> start(Database.BatchableContext BC) {
        List<properto__Sales_Process__c> salesProcesses = [
                SELECT Id, isSynchronizedWithAx__c
                FROM properto__Sales_Process__c
                WHERE Id IN :salesProcessesIds
        ];
        return salesProcesses;

    }
    global void execute(Database.BatchableContext bc, List<properto__Sales_Process__c> salesProcesses) {
        if (salesProcesses != null && salesProcesses.size() > 0) {
            for (properto__Sales_Process__c process : salesProcesses) {
                process.isSynchronizedWithAx__c = true;
                salesProcessesToUpdate.add(process);
            }
        }
    }
    global void finish(Database.BatchableContext bc) {
        Database.update(salesProcessesToUpdate);
    }
}