/**
 * Created by adam on 01/10/18.
 */

public with sharing class INT_AgreementDto_Cordia {


    public String custAccount { get; set; }

    public List<CustomerShares> customers { get; set; }

    public List<INT_AgreementLineDto_Cordia> lines { get; set; }

    public String companyId { get; set; }
    @TestVisible
    public String salesAgreementId { get; set; }
    @TestVisible
    public String languageId { get; set; }
    @TestVisible
    public String currencyIso { get; set; }
    @TestVisible
    public String effectiveDate { get; set; }
    @TestVisible
    public String expirationDate { get; set; }
    @TestVisible
    public String reservationAgreementSignDate { get; set; }
    @TestVisible
    public String preliminarySignDate { get; set; }
    @TestVisible
    public String finalContractSignDate { get; set; }
    @TestVisible
    public String documentTitle { get; set; }
    @TestVisible
    public LegalType legalType { get; set; }
    @TestVisible
    public String originalContractId { get; set; }
    @TestVisible
    public String developmentProject { get; set; }
    @TestVisible
    public String retentionSetupCode { get; set; }
    @TestVisible
    public String defaultAgreementLineType { get; set; }
    @TestVisible
    public Status status { get; set; }
    @TestVisible
    public String salesResponsible { get; set; }
    @TestVisible
    public String cRMResponsible { get; set; }

    public enum Status {
        OnHold, Effective, Canceled, Finished
    }

    public enum LegalType {
        Reservation_cust, Final_cust, Preliminary_cust
    }

    public INT_AgreementDto_Cordia(properto__Sales_Process__c processToSend) {
/*        if (processToSend.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)
                || processToSend.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                properto.CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL)
                || processToSend.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                properto.CommonUtility.SALES_PROCESS_TYPE_OFFER)) {
        } else {

            throw new INT_ValidationException('Sales Process record type not supported');
        }*/

        properto__Sales_Process__c process = [
                SELECT Id, RecordType.DeveloperName, properto__Date_of_signing__c, properto__Contact__c,
                        properto__Status__c, Name, properto__Offer__r.Name, isSynchronizedWithAx__c, RecordTypeId,
                        CurrencyIsoCode, properto__Offer__c, properto__Report_HO_investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c,
                        properto__Report_HO_investment__r.Name, properto__Date_of_signing_Reservation__c, properto__Offer_Line_to_Sale_Term__r.properto__Reservation_Date__c,
                        properto__Date_of_Signing_Final_to_OL__c, properto__Report_HO_investment__r.Id, properto__Agreement__r.properto__Reservation_Date__c,
                        properto__Contact__r.properto__Languages_Package2__c, properto__Contact__r.Account.properto__Country_Of_Origin__c,
                        properto__Account_from_Customer_Group__r.Id, properto__Main_Resource__r.RecordType.DeveloperName,
                        properto__Net_Price_With_Discount__c, properto__Agreement__r.Id, properto__Stage__r.properto__Investment_ID__c,
                        properto__Offer_Line_Resource__r.properto__Bank_Account_Number__c, properto__Agreement__c, properto__Agreement_Number__c,
                        properto__Net_Agreement_Price__c, properto__Price_With_Discount__c, properto__Main_Resource__r.properto__Bank_Name__c,
                        properto__Report_HO_investment__r.properto__Investment_Shortcut__c, properto__Handover_Protocol__c,
                        properto__Resources_for_event__c, properto__Max_Date_of_Developer_Agreement__c
                FROM properto__Sales_Process__c
                WHERE Id = :processToSend.Id
        ];
        System.debug('process: ' + process.properto__Main_Resource__r.RecordType.DeveloperName);

        properto__Sales_Process__c reservationAgreement = [
                SELECT Id,properto__Date_of_signing_Reservation__c, CreatedDate
                FROM properto__Sales_Process__c
                WHERE properto__Offer__c = :process.properto__Offer__c
                AND RecordType.DeveloperName = 'Preliminary_Agreement'
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];
        System.debug('reservation agreement: ' + reservationAgreement);


        List<properto__Sales_Process__c> allRelatedSalesProcesses = [
                SELECT Id, RecordType.DeveloperName, properto__Date_of_signing__c, properto__Contact__c,
                        properto__Status__c, CurrencyIsoCode
                FROM properto__Sales_Process__c
                WHERE properto__Offer__c = :process.properto__Offer__c
        ];

        Map<String, properto__Sales_Process__c> recordTypeSalesProcessMap = getRecordTypeSalesProcessMap(allRelatedSalesProcesses);
        recordTypeSalesProcessMap.put(process.RecordType.DeveloperName, process);
        Account customerAccount = getClientAccountFromProcess(process);

        this.lines = getAgreementLines(process);


        setCustomersSharesAndMainCustomer(process);
        //no
        this.companyId = process.properto__Report_HO_investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c;
        //no
        this.salesAgreementId = INT_CordiaHelper.salesProcessToCordiaAgreementId(process.properto__Offer__r.Name,
                this.companyId);
        //no
        this.languageId = INT_CordiaHelper.languageToDocumentLanguageCode(process.properto__Contact__r.Account.properto__Country_Of_Origin__c);
        //no
        this.currencyIso = process.CurrencyIsoCode;

        if (process.properto__Date_of_signing__c != null) {
            this.effectiveDate = String.valueOf(process.properto__Date_of_signing__c);
        } else if (process.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT && process.properto__Date_of_signing_Reservation__c != null) {
            this.effectiveDate = String.valueOf(process.properto__Date_of_signing_Reservation__c);
        } else {
            effectiveDate = String.valueOf(Date.today());
        }
        //yes
        if (process.RecordType.DeveloperName == 'Preliminary_Agreement') {
            this.expirationDate = String.valueOf(process.properto__Max_Date_of_Developer_Agreement__c);
        }
        //yes
        if (process.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_HANDOVER_PROTOCOL) {
            this.reservationAgreementSignDate = String.valueOf(reservationAgreement.properto__Date_of_signing_Reservation__c.format('YYYY-MM-dd'));
        } else if (process.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT) {
            this.reservationAgreementSignDate = String.valueOf(process.properto__Date_of_signing_Reservation__c.format('YYYY-MM-dd'));
        }else if(process.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT)){
            this.reservationAgreementSignDate = String.valueOf(reservationAgreement.properto__Date_of_signing_Reservation__c.format('YYYY-MM-dd'));
        }
        //yes
        setPreliminarySignDate(recordTypeSalesProcessMap);
        //yes
        setFinalSignDate(recordTypeSalesProcessMap);
        //yes
        this.documentTitle = process.properto__Agreement_Number__c;
        //no
        this.legalType = getLegalTypeFromProcessStatus(process);
        //yes
        //this.originalContractId; //not needed
        //no
        this.developmentProject = process.properto__Stage__r.properto__Investment_ID__c;
        //yes
        //this.retentionSetupCode; // not needed
        //yes
        this.status = getAxStatusBasedOnPropertoStatus(process.properto__Status__c);
        //yes
        //this.salesResponsible; // moze byc puste


    }


    public class CustomerShares {

        public Decimal share { get; set; }

        public Id custAccount { get; set; }

    }

    private void setCustomersSharesAndMainCustomer(properto__Sales_Process__c salesProcess) {
        if (salesProcess.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT ||
                salesProcess.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) {
            System.debug('inside preliminary get customers');
            List<properto__Sales_Process__c> subSalesProcessCustomerGroup = [
                    SELECT Id, properto__Share__c, properto__Account_from_Customer_Group__r.Id, properto__Main_Customer__c
                    FROM properto__Sales_Process__c
                    WHERE properto__Proposal_from_Customer_Group__c = :salesProcess.properto__Offer__c
            ];
            System.debug('subsales: ' + subSalesProcessCustomerGroup);
            List<CustomerShares> customerSharesList = new List<INT_AgreementDto_Cordia.CustomerShares>();
            for (properto__Sales_Process__c subSalesProcess : subSalesProcessCustomerGroup) {
                if (subSalesProcess.properto__Main_Customer__c == true) {
                    this.custAccount = subSalesProcess.properto__Account_from_Customer_Group__r.Id;
                }
                CustomerShares customerShares = new CustomerShares();
                customerShares.custAccount = subSalesProcess.properto__Account_from_Customer_Group__r.Id;
                customerShares.share = subSalesProcess.properto__Share__c;
                customerSharesList.add(customerShares);
            }
            this.customers = customerSharesList;
            System.debug('customer shares list: ' + customerSharesList);
        } else {
            List<properto__Sales_Process__c> subSalesProcessCustomerGroup = [
                    SELECT Id, properto__Share__c, properto__Account_from_Customer_Group__r.Id, properto__Main_Customer__c
                    FROM properto__Sales_Process__c
                    WHERE RecordTypeId = :properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                            properto.CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP)
                    And properto__After_sales_from_Customer_Group__c = :salesProcess.Id
            ];
            System.debug(subSalesProcessCustomerGroup);
            List<CustomerShares> customerSharesList = new List<INT_AgreementDto_Cordia.CustomerShares>();
            for (properto__Sales_Process__c subSalesProcess : subSalesProcessCustomerGroup) {
                if (subSalesProcess.properto__Main_Customer__c == true) {
                    this.custAccount = subSalesProcess.properto__Account_from_Customer_Group__r.Id;
                }
                CustomerShares customerShares = new CustomerShares();
                customerShares.custAccount = subSalesProcess.properto__Account_from_Customer_Group__r.Id;
                customerShares.share = subSalesProcess.properto__Share__c;
                customerSharesList.add(customerShares);
            }
            this.customers = customerSharesList;
        }

    }


    @TestVisible
    private void setPreliminarySignDate(Map<String, properto__Sales_Process__c> processMap) {
        try {
            if (processMap.get('Preliminary_Agreement').properto__Date_of_signing__c != null) {
                this.preliminarySignDate = String.valueOf(processMap.get('Preliminary_Agreement').properto__Date_of_signing__c);
            }
        } catch (NullPointerException nullPointerException) {
            //can be empty
        }
    }
    @TestVisible
    private void setFinalSignDate(Map<String, properto__Sales_Process__c> processMap) {
        try {
            if (processMap.get('Final_Agreement').properto__Date_of_signing__c != null) {
                this.finalContractSignDate = String.valueOf(processMap.get('Final_Agreement').properto__Date_of_signing__c);
            }
        } catch (NullPointerException nullPointerException) {
            //can be empty
        }
    }
    @TestVisible
    private void setReservationAgreementSignDate(Map<String, properto__Sales_Process__c> processMap) {
        try {
            if (processMap.get('Reservation_Agreement').properto__Date_of_signing__c != null) {
                this.reservationAgreementSignDate = String.valueOf(processMap.get('Reservation_Agreement').properto__Date_of_signing__c);
            }
        } catch (NullPointerException nullPointerException) {
            //can be empty
        }
    }
    @TestVisible
    private static Map<String, properto__Sales_Process__c> getRecordTypeSalesProcessMap(List<properto__Sales_Process__c> processes) {

        Map<String, properto__Sales_Process__c> processesByRecordTypes = new Map<String, properto__Sales_Process__c>();
        for (properto__Sales_Process__c process : processes) {
            processesByRecordTypes.put(process.RecordType.DeveloperName, process);
        }
        return processesByRecordTypes;
    }

    @TestVisible
    private static Account getClientAccountFromProcess(properto__Sales_Process__c process) {

        Account accounts = [
                SELECT Id
                FROM Account
                WHERE Id = :[SELECT AccountId FROM Contact WHERE Id = :process.properto__Contact__c].AccountId
        ].get(0);
        return accounts;
    }

    public static List<INT_AgreementLineDto_Cordia> getAgreementLines(properto__Sales_Process__c salesProcess) {
        SYstem.debug('payments, salesProcess recordType: ' + salesProcess.RecordType.DeveloperName);
        List<properto__Payment__c> payments;
        if (salesProcess.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT) {

            payments = [
                    SELECT Id, properto__Contact__c, properto__Agreements_Installment__c, RecordType.DeveloperName, CurrencyIsoCode,
                            properto__Payment_For__c, properto__Due_Date__c, properto__Bank_Account_For_Resource__c, properto__Payment_For__r.Name,
                            properto__Payment_For__r.RecordType.DeveloperName, properto__Primary_Vat__c, properto__Net_Amount__c, properto__Amount_to_pay__c,
                            properto__Offer__c, properto__Base_Schedule_Installment__r.properto__Milestone__c

                    FROM properto__Payment__c
                    WHERE properto__Agreements_Installment__c = :salesProcess.Id AND (RecordType.DeveloperName = 'Deposit' OR RecordType.DeveloperName = 'Installment')
            ];
            System.debug('payment from preliminary:' + payments);
        } else if (salesProcess.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_FINAL_AGREEMENT) {
            payments = [
                    SELECT Id, properto__Contact__c, properto__Agreements_Installment__c, RecordType.DeveloperName, CurrencyIsoCode,
                            properto__Payment_For__c, properto__Due_Date__c, properto__Bank_Account_For_Resource__c, properto__Payment_For__r.Name,
                            properto__Payment_For__r.RecordType.DeveloperName, properto__Primary_Vat__c, properto__Net_Amount__c, properto__Amount_to_pay__c,
                            properto__Offer__c, properto__Base_Schedule_Installment__r.properto__Milestone__c, (
                            SELECT Id, properto__Agreements_Installment__c, RecordType.DeveloperName, CurrencyIsoCode,
                                    properto__Milestone__c, properto__Payment_For__r.Name, properto__Primary_Vat__c,
                                    properto__Payment_For__c, properto__Due_Date__c, properto__Bank_Account_For_Resource__c,
                                    properto__Offer__c, properto__Contact__r.Id, properto__Contact__c,
                                    properto__Sub_Installment_to_Installment__r.properto__Base_Schedule_Installment__r.properto__Milestone__c,
                                    properto__Payment_For__r.RecordType.DeveloperName, properto__Contact__r.AccountId,
                                    properto__Net_Amount__c, properto__Amount_to_pay__c
                            FROM properto__Sub_Installments__r
                    )
                    FROM properto__Payment__c
                    WHERE properto__After_sales_Service__c = :salesProcess.properto__Handover_Protocol__c AND (RecordType.DeveloperName = 'Deposit'OR RecordType.DeveloperName = 'Installment')
            ];
            System.debug('payment from final:' + payments);
        } else {
            payments = [
                    SELECT Id, properto__Contact__c, properto__Agreements_Installment__c, RecordType.DeveloperName, CurrencyIsoCode,
                            properto__Payment_For__c, properto__Due_Date__c, properto__Bank_Account_For_Resource__c, properto__Payment_For__r.Name,
                            properto__Payment_For__r.RecordType.DeveloperName, properto__Primary_Vat__c, properto__Net_Amount__c, properto__Amount_to_pay__c,
                            properto__Offer__c, properto__Base_Schedule_Installment__r.properto__Milestone__c, (
                            SELECT Id, properto__Agreements_Installment__c, RecordType.DeveloperName, CurrencyIsoCode,
                                    properto__Milestone__c, properto__Payment_For__r.Name, properto__Primary_Vat__c,
                                    properto__Payment_For__c, properto__Due_Date__c, properto__Bank_Account_For_Resource__c,
                                    properto__Offer__c, properto__Contact__r.Id, properto__Contact__c,
                                    properto__Sub_Installment_to_Installment__r.properto__Base_Schedule_Installment__r.properto__Milestone__c,
                                    properto__Payment_For__r.RecordType.DeveloperName, properto__Contact__r.AccountId,
                                    properto__Net_Amount__c, properto__Amount_to_pay__c
                            FROM properto__Sub_Installments__r
                    )
                    FROM properto__Payment__c
                    WHERE properto__After_sales_Service__c = :salesProcess.Id AND (RecordType.DeveloperName = 'Installment' Or RecordType.DeveloperName = 'Deposit')
            ];
        }

        List<INT_AgreementLineDto_Cordia> agreementLineDtos = new List<INT_AgreementLineDto_Cordia>();
        for (properto__Payment__c payment : payments) {
            if (payment.properto__Sub_Installments__r.size() > 0) {
                agreementLineDtos.addAll(subPaymentsToAgreementLines(payment.properto__Sub_Installments__r, payment.properto__Contact__c,
                        salesProcess, payment.properto__Base_Schedule_Installment__r.properto__Milestone__c));
            } else {

                agreementLineDtos.add(new INT_AgreementLineDto_Cordia(payment, salesProcess));
            }
        }
        return agreementLineDtos;

    }


    public static List<INT_AgreementLineDto_Cordia> subPaymentsToAgreementLines(List<properto__Payment__c> payments,
            Id contactId, properto__Sales_Process__c salesProcess, String milestone) {
        List<INT_AgreementLineDto_Cordia> agreementLineDtoCordias = new List<INT_AgreementLineDto_Cordia>();
        for (properto__Payment__c payment : payments) {
            payment.properto__Milestone__c = milestone;
            payment.properto__Contact__c = contactId;
            agreementLineDtoCordias.add(new INT_AgreementLineDto_Cordia(payment, salesProcess));
        }
        return agreementLineDtoCordias;
    }

    @TestVisible
    //Legal Type translation
    public static LegalType getLegalTypeFromProcessStatus(properto__Sales_Process__c process) {
        Map<String, LegalType> axLegalTypePropertoStatusMap = getLegalTypeStatusMap();
        if (axLegalTypePropertoStatusMap.get(process.RecordType.DeveloperName) == null) {
            throw new INT_ValidationException('Sales Process record Type is bad: ' + process.RecordType.DeveloperName + '. Record Type type cannot empty');
        } else {
            return axLegalTypePropertoStatusMap.get(process.RecordType.DeveloperName);
        }
    }

    private static Map<String, LegalType> getLegalTypeStatusMap() {
        Map<String, LegalType> legalTypeStatusMap = new Map<String, LegalType>();
        legalTypeStatusMap.put('Offer', LegalType.Reservation_cust);
        legalTypeStatusMap.put('Final_Agreement', LegalType.Final_cust);
        legalTypeStatusMap.put('Hand_over_Protocol', LegalType.Preliminary_cust);
        legalTypeStatusMap.put('Preliminary_Agreement', LegalType.Reservation_cust);

        return legalTypeStatusMap;
    }


    //Ax status translation
    @TestVisible
    public static Status getAxStatusBasedOnPropertoStatus(String status) {
        Map<String, Status> axPropertoStatusMap = getAxStatusMap();
        return axPropertoStatusMap.get(status);
    }

    private static Map<String, Status> getAxStatusMap() {
        Map<String, Status> statusAxMap = new Map<String, Status>();
        //On Hold
        statusAxMap.put('Waiting for customer approval', Status.OnHold);
        statusAxMap.put('Waiting for manager approval', Status.OnHold);
        statusAxMap.put('Not accepted by manager', Status.OnHold);
        statusAxMap.put('Rejected by customer', Status.OnHold);
        statusAxMap.put('Out of date', Status.OnHold);
        statusAxMap.put('Not signed - defects', Status.OnHold);
        statusAxMap.put('Sent to sign', Status.OnHold);

        //Effective
        statusAxMap.put('New', Status.Effective);
        statusAxMap.put('Prepering to final agreement', Status.Effective);
        statusAxMap.put('Accepted by customer', Status.Effective);
        statusAxMap.put('Handover protocol signed', Status.Effective);
        statusAxMap.put('Reservation Agreement signed', Status.Effective);
        statusAxMap.put('Annex Prepering', Status.Effective);
        statusAxMap.put('Accepted by manager', Status.Effective);
        statusAxMap.put('Preparation of a statement for a notary public', Status.Effective);
        statusAxMap.put('Preparing Developer/ Preliminary Agreement', Status.Effective);
        statusAxMap.put('Approval Recalled', Status.Effective);
        statusAxMap.put('Annex/Settlement Singed', Status.Effective);
        statusAxMap.put('Developer/ Preliminary Agreement signed', Status.Effective);
        statusAxMap.put('Signed', Status.Effective);
        statusAxMap.put('Signed - no defects', Status.Effective);
        statusAxMap.put('Signed - with defects', Status.Effective);
        statusAxMap.put('Proposed', Status.Effective);
        statusAxMap.put('Accepted ', Status.Effective);
        statusAxMap.put('Received', Status.Effective);
        statusAxMap.put('Rejected', Status.Effective);

        //Canceled
        statusAxMap.put('Cancellation', Status.Canceled);
        statusAxMap.put('Cancellation - copy', Status.Canceled);
        statusAxMap.put('Resignation', Status.Canceled);
        statusAxMap.put('Lost', Status.Canceled);

        //Finished
        statusAxMap.put('Sold', Status.Finished);
        statusAxMap.put('Final Agreement Signed', Status.Finished);

        return statusAxMap;
    }
}