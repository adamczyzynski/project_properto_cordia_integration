/**
 * Created by adam on 16/08/18.
 */

public with sharing class INT_AgreementLineDto_Cordia {

    private String customerAccountId;
    private String company;
    private String salesAgreementId;
    private String itemNo; // Can be empty
    private Integer quantity; // sprawdzic poprawnosc
    private Decimal amount;
    private String bankAccountCode;
    private String currencyIso;
    private String projectPhase;
    private String paymentCategory;
    private String property;
    private String dueDate;
    private String notLaterThanDate; //  moze byc puste
    private String cFCode;
    private Integer vatPct;

    public INT_AgreementLineDto_Cordia(properto__Payment__c payment, properto__Sales_Process__c process) {
        Contact contact = getContactFromPayment(payment);
        Account customerAccount = getCustomerAccountFromContact(contact);
        this.customerAccountId = customerAccount.Id;
        this.company = process.properto__Report_HO_investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c;
        this.salesAgreementId = INT_CordiaHelper.salesProcessToCordiaAgreementId(process.properto__Offer__r.Name,
                process.properto__Report_HO_investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c);
        this.amount = payment.properto__Amount_to_pay__c;
        this.bankAccountCode = payment.properto__Bank_Account_For_Resource__c;
        setQuantity(payment);

        this.paymentCategory = getPaymentCategory(payment);
        this.currencyIso = payment.CurrencyIsoCode;
        try {
            if (payment.RecordType.DeveloperName == 'Deposit') {
                this.projectPhase = 'Deposit';
            } else if (payment.properto__Base_Schedule_Installment__r.properto__Milestone__c != null
                    && process.RecordType.DeveloperName != 'Preliminary_Agreement') {
                this.projectPhase = payment.properto__Base_Schedule_Installment__r.properto__Milestone__c;
            } else if (process.RecordType.DeveloperName != 'Preliminary_Agreement') {
                this.projectPhase = payment.properto__Milestone__c;
            } else if (process.RecordType.DeveloperName == 'Preliminary_Agreement') {
                this.projectPhase = 'Deposit';
            }
        } catch (SObjectException sObjectException) {
            throw new INT_ValidationException('Integration exception: Milestone cannot be empty, check base schedule installment. ' + sObjectException.getMessage());
        }

        if (process.RecordType.DeveloperName == 'Preliminary_Agreement') {
            this.property = process.properto__Resources_for_event__c;
        } else {
            this.property = payment.properto__Payment_For__r.Name;
        }


        this.dueDate = String.valueOf(payment.properto__Due_Date__c);

        if (payment.RecordType.DeveloperName == properto.CommonUtility.PAYMENT_TYPE_DEPOSIT) {
            this.cFCode = getCfId().get('Deposit');
        } else if (process.RecordType.DeveloperName == properto.CommonUtility.SALES_PROCESS_TYPE_PRELIMINARY_AGREEMENT) {
            try {
                properto__Resource__c mainResourceForDeposit = [
                        SELECT RecordType.DeveloperName
                        FROM properto__Resource__c
                        WHERE Name = :process.properto__Resources_for_event__c
                ];
                this.cFCode = getCfIdByPayment(mainResourceForDeposit.RecordType.DeveloperName);
            } catch (QueryException exceptionQueryException) {
                if (process.properto__Resources_for_event__c != null) {
                    this.cFCode = getCfIdByPayment('Other_Resources');
                } else {
                    throw new INT_ValidationException('main resource cannot be empty');
                }
            }
        } else {
            this.cFCode = getCfIdByPayment(payment);
        }


        if (payment.properto__Primary_Vat__c != null) {
            this.vatPct = Integer.valueOf(payment.properto__Primary_Vat__c);
        } else if (payment.properto__Amount_to_pay__c != null && payment.properto__Net_Amount__c != null) {
            this.vatPct = INT_CordiaHelper.getPercentFromGrossNett(payment.properto__Amount_to_pay__c, payment.properto__Net_Amount__c);
        }


    }

    private static Decimal getNetValue(properto__Sales_Process__c process) {
        if (process.properto__Net_Price_With_Discount__c != null) {
            return process.properto__Net_Price_With_Discount__c;
        } else if (process.properto__Net_Price_With_Discount__c == null && process.properto__Net_Agreement_Price__c != null) {
            return process.properto__Net_Agreement_Price__c;
        } else {
            throw new INT_ValidationException('AgreementLineDto: unit price net cannot be empty. Problem with SalesProcess id: ' + process.Id);
        }
    }

    private static Account getCustomerAccountFromContact(Contact contact) {
        System.debug('contact : ' + contact);
        Account account = [
                SELECT Id, Name
                FROM Account
                WHERE Id = :contact.AccountId
        ];
        return account;
    }
    private static Contact getContactFromPayment(properto__Payment__c payment) {
        if (payment.properto__Contact__c != null) {
            Contact contact = [
                    SELECT Id, AccountId, properto__Language_package__c, CurrencyIsoCode
                    FROM Contact
                    WHERE Id = :payment.properto__Contact__c
            ];
            return contact;
        } else if (payment.properto__Contact__r.Id != null) {
            Contact contact = [
                    SELECT Id, AccountId, properto__Language_package__c, CurrencyIsoCode
                    FROM Contact
                    WHERE Id = :payment.properto__Contact__r.Id
            ];
            return contact;
        } else {
            return null;
        }
    }
//TODO do dyskusji
    private void setQuantity(properto__Payment__c payment) {
        this.quantity = ([
                SELECT Id
                FROM properto__Sales_Process__c
                WHERE RecordType.DeveloperName = 'Offer_Line'
                AND properto__Offer_Line_to_Sale_Term__c = :payment.properto__Agreements_Installment__c
        ].size());
    }
//TODO przerobic aby korzystalo z helpera
    private static String getPaymentCategory(properto__Payment__c payment) {
        return getPaymentCategoryMap().get(payment.RecordType.DeveloperName);
    }

    private static Map<String, String> getPaymentCategoryMap() {
        Map<string, String> paymentCategoryMap = new Map<String, String >();
        paymentCategoryMap.put('Installment', 'NORMAL');
        paymentCategoryMap.put('Sub_installment', 'NORMAL');
        paymentCategoryMap.put('Deposit', 'RESERV_DEP');
        paymentCategoryMap.put('Installment_Change', 'NORMAL');
        paymentCategoryMap.put('Reimbursement', 'NORMAL');
        paymentCategoryMap.put('Overpayment', 'NORMAL');
        paymentCategoryMap.put('Expense', 'NORMAL');
        paymentCategoryMap.put('Interest_Note', 'NORMAL');

        return paymentCategoryMap;
    }

    private static String getCfIdByPayment(properto__Payment__c payment) {
        return getCfId().get(payment.properto__Payment_For__r.RecordType.DeveloperName);
    }
    private static String getCfIdByPayment(String paymentRecordTypeDeveloperName) {
        return getCfId().get(paymentRecordTypeDeveloperName);
    }

    private static Map<String, String> getCfId() {
        Map<String, String> cfIdMap = new Map<String, String>();
//cfIdMap.put('Building','');
//cfIdMap.put('Commercial_Property',);
        cfIdMap.put('Flat_Apartment', '1_4_06');
        cfIdMap.put('Other_Resources', '1_4_09');
        cfIdMap.put('Deposit', '1_4_09');
        cfIdMap.put('Parking_Space', '1_4_05');
        cfIdMap.put('Storage', '1_4_08');
        return cfIdMap;
    }

}