/**
 * Created by adam on 20/08/18.
 */

public with sharing class INT_CustomerHttpService {

    public static String generateJsonCustomerDto(List<Account> accounts){
        List<INT_CustomerDto_Cordia> customerDto_cordias = INT_CustomerDto_Cordia.accountsListToCustomerList(accounts);
        String JString = JSON.serialize(customerDto_cordias);
        return  JString;

    }
    public static String generateJsonCustomerDto(Account accounts){
        INT_CustomerDto_Cordia customerDto_cordias = INT_CustomerDto_Cordia.accountToCustomer(accounts);
        String JString = JSON.serialize(customerDto_cordias);
        return  JString;

    }

}