/**
 * Created by adam on 18/10/18.
 */

public without sharing class INT_Invoice_Service_Cordia {
    final static String endpoint = 'http://54.38.136.127:8081/invoice';
    final static String path = '/invoice';

    private List<INT_Invoice_Cordia> invoicesToSend;


    public INT_Invoice_Service_Cordia(List<properto__Invoice__c> invoices) {
        this.invoicesToSend = new List<INT_Invoice_Cordia>();
        for (properto__Invoice__c invoice : invoices) {
            this.invoicesToSend.add(new INT_Invoice_Cordia(invoice));
        }
    }
    public static void sendInvoice(INT_Invoice_Cordia intInvoiceCordia){
        String jsonInvoiceString = JSON.serialize(intInvoiceCordia);
        INT_HttpClient_Cordia httpClientCordia = new INT_HttpClient_Cordia(path, 'POST', jsonInvoiceString);
        System.debug('invoice json: '+jsonInvoiceString);
        httpClientCordia.setHeader('Content-Type', 'application/json');
        HttpResponse httpResponse = httpClientCordia.send();
        System.debug('response: ' + httpResponse);
    }

    public void sendInvoices() {
        if (!Test.isRunningTest()) {
            for(INT_Invoice_Cordia intInvoiceCordia: this.invoicesToSend){
                sendInvoice(intInvoiceCordia);
            }

        }
    }
}