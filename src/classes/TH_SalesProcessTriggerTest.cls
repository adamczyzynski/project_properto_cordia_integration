/**
 * Created by adam on 09/01/19.
 */

@IsTest
public class TH_SalesProcessTriggerTest {

    @IsTest
    public static void salesProcesCanBeUsedToSyncWithAx(){
        properto__Sales_Process__c oldSalesProcess = new properto__Sales_Process__c();
        properto__Sales_Process__c newSalesProcess = new properto__Sales_Process__c();

        oldSalesProcess.isSynchronizedWithAx__c = false;
        newSalesProcess.isSynchronizedWithAx__c = false;

        System.assertEquals(true, TH_SalesProcessTrigger.canBeUsedToSyncWithAx(oldSalesProcess, newSalesProcess));

        oldSalesProcess.isSynchronizedWithAx__c = false;
        newSalesProcess.isSynchronizedWithAx__c = true;
        System.assertEquals(false, TH_SalesProcessTrigger.canBeUsedToSyncWithAx(oldSalesProcess, newSalesProcess));

        oldSalesProcess.isSynchronizedWithAx__c = true;
        newSalesProcess.isSynchronizedWithAx__c = true;
        System.assertEquals(false, TH_SalesProcessTrigger.canBeUsedToSyncWithAx(oldSalesProcess, newSalesProcess));

        oldSalesProcess.isSynchronizedWithAx__c = true;
        newSalesProcess.isSynchronizedWithAx__c = false;
        System.assertEquals(false, TH_SalesProcessTrigger.canBeUsedToSyncWithAx(oldSalesProcess, newSalesProcess));

    }
}