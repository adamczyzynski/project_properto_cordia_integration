/**
 * Created by adam on 07/01/19.
 */

@IsTest
public class INT_AgreementTestIT {

    @TestSetup
    static void prepare() {
        properto__ESBhost__c esbHost = new properto__ESBhost__c();
        esbHost.ESBUserAndPassword__c = 'testPass';
        esbHost.properto__Host__c = 'http://0.0.0.1:8081';
        esbHost.Name = 'prod';
        insert esbHost;
    }

    @IsTest
    public static void shouldCreateCorrectReservationAgreementWhenOneClient() {

        INT_HttpClient_Cordia cordia = new INT_HttpClient_Cordia('test', 'POST', 'test');
        HttpResponse response = cordia.send();
        System.debug('code:' + response.getStatusCode());

    }

}