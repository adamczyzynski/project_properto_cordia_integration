/**
 * Created by adam on 18/10/18.
 */

public without sharing class INT_Invoice_Cordia {

    public String companyId { get; set; }

    public String custAccount { get; set; }

    public String invoiceId { get; set; }

    public String description { get; set; }

    public String currencyIso { get; set; }

    public String transDate { get; set; }

    public String issueDate { get; set; }

    public String vatDate { get; set; }

    public String dueDate { get; set; }

    public Decimal amount { get; set; }

    public String reasonCode { get; set; }

    public String salesAgreementId { get; set; }

    public String cfCode { get; set; }

    public String developmentProject { get; set; }

    public String property { get; set; }

    public Decimal vatAmount { get; set; }

    public String invoiceType { get; set; }

    public List<INT_InvoiceLineDto_Cordia> lines { get; set; }


    public INT_Invoice_Cordia(properto__Invoice__c newInvoice) {

        properto__Invoice__c invoice = [
                SELECT Id, properto__Agreement__c, Name, CurrencyIsoCode, properto__Client__r.Id,
                        properto__Client__r.properto__Country_Of_Origin__c, properto__VAT__c, RecordType.DeveloperName,
                        properto__Invoice_For__r.properto__VAT__c, CreatedDate, properto__Gross_Value__c, properto__Due_date__c,
                        properto__Invoice_For__r.Id, properto__Invoice_For__r.Name, properto__VAT_Amount__c,
                        properto__Invoice_For__r.properto__Account__r.Name, properto__Invoice_For__r.properto__Investment__r.Name,
                        properto__Description__c, properto__Invoice_For__r.properto__Material_Index_SAP__c,
                        properto__Invoice_Date__c, properto__Sale_Date__c, properto__Payment_Deadline__c,
                        properto__Invoice_For__r.properto__Account__r.properto__Short_name_of_the_Investor__c,
                        properto__Agreement__r.Id, properto__Agreement__r.properto__Offer__r.Name,
                        properto__Invoice_For__r.RecordType.DeveloperName, properto__Agreement__r.properto__Stage__r.properto__Investment_ID__c,
                        properto__Invoice_For__r.properto__Investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c,
                        properto__Corrected_Invoice__r.RecordType.DeveloperName, properto__Invoice_number__c
                FROM properto__Invoice__c
                WHERE Id = :newInvoice.Id
        ];
        this.companyId = invoice.properto__Invoice_For__r.properto__Investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c;
        this.custAccount = invoice.properto__Client__r.Id;
        this.invoiceId = invoice.properto__Invoice_number__c;
        this.description = invoice.properto__Description__c;
        this.currencyIso = invoice.CurrencyIsoCode;
        this.transDate = String.valueOf(invoice.properto__Invoice_Date__c);
        this.issueDate = String.valueOf(invoice.properto__Sale_Date__c);
        this.vatDate = String.valueOf(invoice.properto__Sale_Date__c);

        if (invoice.properto__Payment_Deadline__c != null) {
            this.dueDate = String.valueOf(invoice.properto__Payment_Deadline__c);
        } else {
            this.dueDate = String.valueOf(invoice.properto__Sale_Date__c);
        }


        this.amount = invoice.properto__Gross_Value__c;
        this.reasonCode = INT_CordiaHelper.getReasonCode(null);
        this.salesAgreementId = INT_CordiaHelper.salesProcessToCordiaAgreementId(invoice.properto__Agreement__r.properto__Offer__r.Name, this.companyId);
        this.cfCode = INT_CordiaHelper.getCfIdByPayment(invoice.properto__Invoice_For__r.RecordType.DeveloperName);
        this.developmentProject = invoice.properto__Agreement__r.properto__Stage__r.properto__Investment_ID__c;
        this.property = invoice.properto__Invoice_For__r.Name;
        this.vatAmount = invoice.properto__VAT_Amount__c;
        this.invoiceType = setInvoiceType(invoice);
        this.lines = getRelatedLines(invoice);
        System.debug('lines: ' + lines);
    }


    public static List<INT_InvoiceLineDto_Cordia> getRelatedLines(properto__Invoice__c advanceInvoice) {
        List<properto__Invoice__c> relatedInvoicesList = relatedInvoiceLines(advanceInvoice);
        return getIntInvoiceLinesBasedOnInvoices(relatedInvoicesList,
                advanceInvoice.properto__Client__r.properto__Country_Of_Origin__c,
                advanceInvoice.properto__Agreement__r.properto__Offer__r.Name);

    }

    private static List<INT_InvoiceLineDto_Cordia> getIntInvoiceLinesBasedOnInvoices(List<properto__Invoice__c> relatedInvoices,
            String countryOfOrigin, String salesAgreementIdCordia) {
        List<INT_InvoiceLineDto_Cordia> relatedLines = new List<INT_InvoiceLineDto_Cordia>();
        if (relatedInvoices != null) {
            for (properto__Invoice__c relatedInvoice : relatedInvoices) {
                INT_InvoiceLineDto_Cordia intInvoiceLineDtoCordia = new INT_InvoiceLineDto_Cordia();
                intInvoiceLineDtoCordia.description = relatedInvoice.properto__Description__c;
                intInvoiceLineDtoCordia.amount = relatedInvoice.properto__Gross_Value__c;
                intInvoiceLineDtoCordia.vatAmount = relatedInvoice.properto__VAT_Amount__c;
                intInvoiceLineDtoCordia.salesTaxGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(countryOfOrigin);
                if (relatedInvoice.properto__VAT__c != null) {
                    intInvoiceLineDtoCordia.itemSalesTaxGroup = INT_CordiaHelper.getItemTaxGroupByVAT(Integer.valueOf(relatedInvoice.properto__VAT__c));
                } else {
                    intInvoiceLineDtoCordia.itemSalesTaxGroup =
                            INT_CordiaHelper.getItemTaxGroupByVAT(
                                    Integer.valueOf(INT_CordiaHelper.getPercentFromGrossNett(relatedInvoice.properto__Gross_Value__c, relatedInvoice.properto__Net_Value__c)));
                }
                intInvoiceLineDtoCordia.reasonCode = INT_CordiaHelper.getReasonCode('');
                intInvoiceLineDtoCordia.finDim_Agreement = INT_CordiaHelper.salesProcessToCordiaAgreementId(salesAgreementIdCordia,
                        relatedInvoice.properto__Invoice_For__r.properto__Investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c);
                intInvoiceLineDtoCordia.cFCode = INT_CordiaHelper.getCfIdByPayment(relatedInvoice.properto__Invoice_For__r.RecordType.DeveloperName);
                intInvoiceLineDtoCordia.developmentProject = relatedInvoice.properto__Invoice_For__r.properto__Stage__r.properto__Investment_ID__c;
                intInvoiceLineDtoCordia.property = relatedInvoice.properto__Invoice_For__r.Name;
                intInvoiceLineDtoCordia.invoiceId = relatedInvoice.Id;

                relatedLines.add(intInvoiceLineDtoCordia);
            }
        }
        return relatedLines;

    }

    private static List<properto__Invoice__c> relatedInvoiceLines(properto__Invoice__c advanceInvoice) {
        List<properto__Invoice__c> invoiceLines = [
                SELECT Id, (
                        SELECT Id, Name, properto__Description__c, properto__Net_Value__c, properto__VAT__c,
                                properto__VAT_Amount__c, properto__Invoice_For__r.Name, properto__Gross_Value__c,
                                properto__Invoice_For__r.properto__Investment__r.Name, properto__Invoice_For__r.RecordType.DeveloperName,
                                properto__Invoice_For__r.properto__Investment__r.properto__Account__r.properto__Short_name_of_the_Investor__c,
                                properto__Invoice_For__r.properto__Stage__r.properto__Investment_ID__c, properto__Invoice_number__c

                        FROM properto__Invoice_Lines__r
                )
                FROM properto__Invoice__c
                WHERE Id = :advanceInvoice.Id
        ].properto__Invoice_Lines__r;

        return invoiceLines;
    }
    @TestVisible
    private static String setInvoiceType(properto__Invoice__c invoice) {
        if (invoice.RecordType.DeveloperName == properto.CommonUtility.INVOICE_TYPE_CORRECTING_INVOICE) {
            return getInvoiceTypeMap().get(invoice.properto__Corrected_Invoice__r.RecordType.DeveloperName);
        } else {
            return getInvoiceTypeMap().get(invoice.RecordType.DeveloperName);
        }

    }
    private static Map<String, String> getInvoiceTypeMap() {
        Map<String, String> invoiceTypeMap = new Map<String, String>();
        invoiceTypeMap.put(properto.CommonUtility.INVOICE_TYPE_ADVANCEINVOICE, INT_CordiaHelper.INVOICE_TYPE_ADVANCE);
        invoiceTypeMap.put(properto.CommonUtility.INVOICE_TYPE_FINAL_INVOICE, INT_CordiaHelper.INVOICE_TYPE_NORMAL);
        invoiceTypeMap.put(properto.CommonUtility.INVOICE_TYPE_SALES_INVOICE, INT_CordiaHelper.INVOICE_TYPE_NORMAL);

        return invoiceTypeMap;
    }
}