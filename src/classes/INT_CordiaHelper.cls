public without sharing class INT_CordiaHelper {

    public static final String INVOICE_TYPE_ADVANCE = 'Advance';
    public static final String INVOICE_TYPE_NORMAL = 'Normal';


    // country of origin in account cannot empty
    public static String getTaxGroupByCountryOfOrigin(String countyOfOrigin) {
        Map<String, String> countriesTaxGroup = getMapTaxCountries();
        if (!countriesTaxGroup.containsKey(countyOfOrigin.toLowerCase())) {
            return 'CUS-NONEU';
        } else {
            return countriesTaxGroup.get(countyOfOrigin.toLowerCase());
        }

    }

    public static String getItemTaxGroupByVAT(Integer vat) {
        Map<Integer, String> itemTaxGroup = getItemSalesGroupMap();
        return itemTaxGroup.get(vat);
    }

    public static String salesProcessToCordiaAgreementId(String salesProcessName, String companyShortName) {
        if(salesProcessName == null){
            throw new INT_ValidationException('Sales Process name cannot be empty');
        }else if(companyShortName == null){
            throw new INT_ValidationException('Company short name cannot be empty');
        }
        return companyShortName + salesProcessName;
    }

    public static Id cordiaAgreementIdToSalesProcessId(String cordiaAgreementId) {
        return [SELECT Id FROM properto__Sales_Process__c WHERE Name = :cordiaAgreementId.substring(4)].Id;
    }

    private static Map<String, String> getMapTaxCountries() {
        Map<String, String> countryTaxGroup = new Map<String, String>();
        countryTaxGroup.put('austria', 'CUS-EU');
        countryTaxGroup.put('belgium', 'CUS-EU');
        countryTaxGroup.put('bulgaria', 'CUS-EU');
        countryTaxGroup.put('cyprus', 'CUS-EU');
        countryTaxGroup.put('czech Republic', 'CUS-EU');
        countryTaxGroup.put('estonia', 'CUS-EU');
        countryTaxGroup.put('finland', 'CUS-EU');
        countryTaxGroup.put('france', 'CUS-EU');
        countryTaxGroup.put('germany', 'CUS-EU');
        countryTaxGroup.put('greece', 'CUS-EU');
        countryTaxGroup.put('hungary', 'Hungarian domestic customer');
        countryTaxGroup.put('ireland', 'CUS-EU');
        countryTaxGroup.put('italy', 'CUS-EU');
        countryTaxGroup.put('latvia', 'CUS-EU');
        countryTaxGroup.put('lithuania', 'CUS-EU');
        countryTaxGroup.put('luxembourg', 'CUS-EU');
        countryTaxGroup.put('malta', 'CUS-EU');
        countryTaxGroup.put('netherlands', 'CUS-EU');
        countryTaxGroup.put('poland', 'CUS-POL');
        countryTaxGroup.put('portugal', 'CUS-EU');
        countryTaxGroup.put('romania', 'CUS-RO');
        countryTaxGroup.put('slovakia', 'CUS-EU');
        countryTaxGroup.put('slovenia', 'CUS-EU');
        countryTaxGroup.put('spain', 'CUS-EU');
        countryTaxGroup.put('sweden', 'CUS-EU');
        countryTaxGroup.put('united kingdom', 'CUS-EU');

        return countryTaxGroup;
    }

    private static Map<Integer, String> getItemSalesGroupMap() {
        Map<Integer, String> itemSalesTaxGroup = new Map<Integer, String>();
        itemSalesTaxGroup.put(0, 'PRODG-0');
        itemSalesTaxGroup.put(5, 'PRODG-MIN');
        itemSalesTaxGroup.put(8, 'PRODG-RED');
        itemSalesTaxGroup.put(23, 'PRODG-FULL');
        return itemSalesTaxGroup;
    }

    private static Map<String, String> getReasonCodesMap() {
        Map<String, String> reasonCodesMap = new Map<String, String>();
        return reasonCodesMap;
    }

    public static String getCfIdByPayment(properto__Payment__c payment) {
        return getCfId().get(payment.properto__Payment_For__r.RecordType.DeveloperName);
    }
    public static String getCfIdByPayment(String paymentRecordTypeDeveloperName) {
        return getCfId().get(paymentRecordTypeDeveloperName);
    }


    private static Map<String, String> getCfId() {
        Map<String, String> cfIdMap = new Map<String, String>();
        //cfIdMap.put('Building','');
        //cfIdMap.put('Commercial_Property',);
        cfIdMap.put('Flat_Apartment', '1_4_06');
        cfIdMap.put('Other_Resources', '1_4_09');
        cfIdMap.put('Parking_Space', '1_4_05');
        cfIdMap.put('Storage', '1_4_08');
        return cfIdMap;
    }

    public static String languageToDocumentLanguageCode(String language) {
        String tempLanguage;
        if (language != null) {
            tempLanguage = language;
            tempLanguage.toLowerCase();
        } else {
            tempLanguage = 'default';
        }
        if (tempLanguage == 'poland') {
            return 'pl';
        } else if (tempLanguage == 'hungary') {
            return 'hu';
        } else if (tempLanguage == 'romania') {
            return 'en-za';
        } else {
            return 'en';
        }
    }

    public static Integer getPercentFromGrossNett(Decimal gross, Decimal nett) {
        return Integer.valueOf(((gross * 100) / nett - 100).round());
    }

    public static String getReasonCode(String valueString) {

        String reasonCode = '';
        switch on valueString {
            when '1' {
                reasonCode = '';
            }
            when else {
                reasonCode = 'CASH11';
            }
        }
        return reasonCode;
    }

}