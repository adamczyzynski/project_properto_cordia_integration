/**
 * Created by adam on 29/11/18.
 */

public without sharing class INT_InvoiceLineDto_Cordia {

    public String description {get; set;}

    public Decimal amount {get; set;}

    public Decimal vatAmount {get; set;}

    public String salesTaxGroup {get; set;}

    public String itemSalesTaxGroup{get; set;}

    public String reasonCode {get; set;}

    public String finDim_Agreement {get; set;}

    public String cFCode {get; set;}

    public String developmentProject {get; set;}

    public String property{get; set;}

    public String invoiceId {get; set;}

}