/**
 * Created by adam on 27/11/18.
 */

public without sharing class INT_CustomerDto_Cordia {

    //no empty
    public Integer dirPartyType { get; set; }
    // 0 - 100
    public String name { get; set; }
    // 0 - 20 AX ID
    public String salesforceCustomerId { get; set; }
    // 0 - 7, no empty
    public String languageId { get; set; }
    // 0 - 250
    public String street { get; set; }
    // 0 - 250, no empty
    public String zipCode { get; set; }
    // 0 - 10, no empty
    public String countryRegionId { get; set; }
    // 0 - 100
    public String city { get; set; }
    // 0 - 10, no empty
    public String customerGroup { get; set; }
    // 0 - 4, no empty
    public String defaultCurrency { get; set; }
    // 0 - 10, no empty
    public String taxGroup { get; set; }
    // 0 - 25, no empty
    public String vatNum { get; set; }
    // 0 - 100
    public String customerBankAccount { get; set; }
    // 0 - 20
    public String customerBankGroup { get; set; }
    // 0 - 100
    public String customerEscrowBankAccount { get; set; }
    // 0 - 20
    public String customerEscrowBankGroup { get; set; }

    public INT_CustomerDto_Cordia() {

    }

    public static List<INT_CustomerDto_Cordia> getAllContactsFromSalesProcess(properto__Sales_Process__c salesProcessIncoming) {
        List<properto__Sales_Process__c> sales_processes = [
                SELECT Id, properto__Account_from_Customer_Group__c, properto__Main_Resource__r.properto__Bank_Account_Number__c
                FROM properto__Sales_Process__c
                WHERE properto__Proposal_from_Customer_Group__c = :salesProcessIncoming.properto__Offer__c
                AND RecordType.DeveloperName = :properto.CommonUtility.SALES_PROCESS_TYPE_CUSTOMER_GROUP
        ];

        List<Id> accountsIdsList = new List<Id>();
        for (properto__Sales_Process__c process : sales_processes) {
            accountsIdsList.add(process.properto__Account_from_Customer_Group__c);
        }

        List<Account> accounts = [
                SELECT Id, RecordType.DeveloperName, properto__Language_package__pc, BillingCity, BillingPostalCode, properto__Country_Of_Origin__c,
                        BillingCountry, properto__Building_No__c, properto__Building_No__pc, properto__Unit_No__c, properto__Unit_No__pc, BillingStreet,
                        CurrencyIsoCode, properto__PESEL__c, properto__PESEL__pc, properto__NIP__c, properto__Bank_Acc_No__c, Name,
                        properto__Bank_Acc_No__pc, properto__Bank_Acc_Name__c, properto__Bank_Acc_Name__pc, FirstName, MiddleName, LastName
                FROM Account
                WHERE Id IN :accountsIdsList
        ];


        Map<Id, properto__Sales_Process__c> accountIdSalesProcessMap = new Map<Id, properto__Sales_Process__c>();

        for (properto__Sales_Process__c process : sales_processes) {
            accountIdSalesProcessMap.put(process.properto__Account_from_Customer_Group__c, process);
        }
        Map<Account, properto__Sales_Process__c> accountToSalesProcessMap = new Map<Account, properto__Sales_Process__c>();
        for (Account account : accounts) {
            accountToSalesProcessMap.put(account, accountIdSalesProcessMap.get(account.Id));
        }

        List<INT_CustomerDto_Cordia> customerDtoCordias = new List<INT_CustomerDto_Cordia>();
        for (Account account : accounts) {
            customerDtoCordias.add(INT_CustomerDto_Cordia.customerDtoCordiaFromSalesProcess(accountToSalesProcessMap.get(account), account));
        }


        return customerDtoCordias;
    }


    public static INT_CustomerDto_Cordia customerDtoCordiaFromSalesProcess(properto__Sales_Process__c processToSend, Account accountFromSalesProcess) {
        properto__Sales_Process__c process = [
                SELECT Id, properto__Contact__c, properto__Offer_Line_Resource__r.properto__Bank_Account_Number__c,
                        properto__Contact__r.Id, properto__Contact_from_Offer_Line__c, RecordType.DeveloperName,
                        properto__Report_HO_investment__r.properto__Current_Bank_Account__c, properto__Offer__r.properto__Main_Resource__r.properto__Bank_Account_Number__c,
                        properto__Report_HO_investment__r.properto__Bank_Account_Number__c, properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Name__c,
                        properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Account_Number__c,
                        properto__Main_Resource__r.properto__Bank_Account_Number__c,
                        properto__Main_Resource__r.properto__Bank_Name__c
                FROM properto__Sales_Process__c
                WHERE Id = :processToSend.Id
        ];
        Account account;
        if (accountFromSalesProcess == null) {
            System.debug('account not from sales process');
            Contact contact = [
                    SELECT Id, AccountId, Name
                    FROM Contact
                    WHERE Id = :process.properto__Contact__c
            ];
            account = [
                    SELECT Id, RecordType.DeveloperName, properto__Language_package__pc, BillingCity, BillingPostalCode, properto__Country_Of_Origin__c,
                            BillingCountry, properto__Building_No__c, properto__Building_No__pc, properto__Unit_No__c, properto__Unit_No__pc, BillingStreet,
                            CurrencyIsoCode, properto__PESEL__c, properto__PESEL__pc, properto__NIP__c, properto__Bank_Acc_No__c,
                            properto__Bank_Acc_No__pc, properto__Bank_Acc_Name__c, properto__Bank_Acc_Name__pc, FirstName, MiddleName, LastName
                    FROM Account
                    WHERE Id = :contact.AccountId
            ];
        } else if (accountFromSalesProcess != null) {
            System.debug('account from salesProcess');
            account = accountFromSalesProcess;
        }
        System.debug('account: '+account);
        INT_CustomerDto_Cordia customerDtoCordia = new INT_CustomerDto_Cordia();
        if (account.RecordType.DeveloperName == 'Company') {
            System.debug('company acc');
            customerDtoCordia = customerFromBusinessAccount(account, process);
        } else if (account.RecordType.DeveloperName == 'Individual_Client') {
            System.debug('personal acc');
            customerDtoCordia = customerFromPersonalAccount(account, process);
        } else {
            System.debug('Integration validation exception for sales process: ' + process);
            throw new INT_ValidationException('Not valid Record Type');
        }
        return customerDtoCordia;
    }

    //Only if customer is update
    public static INT_CustomerDto_Cordia accountToCustomer(Account account) {
        account = [
                SELECT Id, RecordType.DeveloperName, properto__Language_package__pc, BillingCity, BillingPostalCode, properto__Country_Of_Origin__c,
                        BillingCountry, properto__Building_No__c, properto__Building_No__pc, properto__Unit_No__c, properto__Unit_No__pc, BillingStreet,
                        CurrencyIsoCode, properto__PESEL__c, properto__PESEL__pc, properto__NIP__c, properto__Bank_Acc_No__c,
                        properto__Bank_Acc_No__pc, properto__Bank_Acc_Name__c, properto__Bank_Acc_Name__pc, FirstName, MiddleName, LastName
                FROM Account
                WHERE Id = :account.Id
        ];

        INT_CustomerDto_Cordia customerDto_cordia = new INT_CustomerDto_Cordia();
        if (account.RecordType.DeveloperName == 'Company') {
            customerDto_cordia.DirPartyType = PartyType.ORGANISATION.ordinal();
        } else if (account.RecordType.DeveloperName == 'Individual_Client') {
            customerDto_cordia.DirPartyType = PartyType.PERSON.ordinal();
        } else {
            throw new INT_ValidationException('Not valid Record Type');
        }
        if (String.isEmpty(account.MiddleName)) {
            customerDto_cordia.Name = account.FirstName + ' ' + account.LastName;
        } else {

            customerDto_cordia.Name = account.FirstName + ' ' + account.MiddleName + ' ' + account.LastName;
        }
        customerDto_cordia.salesforceCustomerId = account.Id;
        customerDto_cordia.languageId = languageToDocumentLanguageCode(account.properto__Country_Of_Origin__c);
        customerDto_cordia.street = getAddress(account);
        customerDto_cordia.zipCode = account.BillingPostalCode;
        customerDto_cordia.countryRegionId = countryToIsoCode(account.properto__Country_Of_Origin__c);
        customerDto_cordia.city = account.BillingCity;
        customerDto_cordia.CustomerGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDto_cordia.defaultCurrency = account.CurrencyIsoCode;
        customerDto_cordia.vatNum = getVatNum(account);
        customerDto_cordia.taxGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDto_cordia.customerBankAccount = account.properto__Bank_Acc_No__c;
        customerDto_cordia.customerBankGroup = account.properto__Bank_Acc_Name__c;
        //customerDto_cordia.customerEscrowBankAccount; -- property
        //customerDto_cordia.customerEscrowBankGroup;  -- property


        return customerDto_cordia;
    }


    private static INT_CustomerDto_Cordia customerFromPersonalAccount(Account account, properto__Sales_Process__c salesProcessToSend) {
        INT_CustomerDto_Cordia customerDtoCordia = new INT_CustomerDto_Cordia();
        customerDtoCordia.dirPartyType = PartyType.PERSON.ordinal();
        if (String.isEmpty(account.MiddleName)) {
            customerDtoCordia.Name = account.FirstName + ' ' + account.LastName;
        } else {

            customerDtoCordia.Name = account.FirstName + ' ' + account.MiddleName + ' ' + account.LastName;
        }
        customerDtoCordia.salesforceCustomerId = account.Id;
        customerDtoCordia.languageId = languageToDocumentLanguageCode(account.properto__Country_Of_Origin__c);
        customerDtoCordia.street = getAddressFromIndividualClient(account);
        customerDtoCordia.zipCode = account.BillingPostalCode;
        customerDtoCordia.countryRegionId = countryToIsoCode(account.properto__Country_Of_Origin__c);
        customerDtoCordia.city = account.BillingCity;
        customerDtoCordia.defaultCurrency = account.CurrencyIsoCode;
        customerDtoCordia.customerGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDtoCordia.defaultCurrency = account.CurrencyIsoCode;
        customerDtoCordia.vatNum = getVatNum(account);
        customerDtoCordia.taxGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDtoCordia.customerBankAccount = account.properto__Bank_Acc_No__c;
        customerDtoCordia.customerBankGroup = account.properto__Bank_Acc_Name__c;
        if (salesProcessToSend.properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Account_Number__c != null) {
            customerDtocordia.customerEscrowBankAccount = salesProcessToSend.properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Account_Number__c;
        } else {
            customerDtocordia.customerEscrowBankAccount = salesProcessToSend.properto__Main_Resource__r.properto__Bank_Account_Number__c;
        }
        if (salesProcessToSend.properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Name__c != null) {
            customerDtocordia.customerEscrowBankGroup = salesProcessToSend.properto__After_sales_from_Customer_Group__r.properto__Main_Resource__r.properto__Bank_Name__c;
        } else {
            customerDtocordia.customerEscrowBankGroup = salesProcessToSend.properto__Main_Resource__r.properto__Bank_Name__c;
        }

        return customerDtoCordia;
    }


    private static INT_CustomerDto_Cordia customerFromBusinessAccount(Account account, properto__Sales_Process__c salesProcessToSend) {
        INT_CustomerDto_Cordia customerDtoCordia = new INT_CustomerDto_Cordia();
        customerDtoCordia.dirPartyType = PartyType.ORGANISATION.ordinal();

        customerDtoCordia.Name = account.Name;
        customerDtoCordia.salesforceCustomerId = account.Id;
        customerDtoCordia.languageId = INT_CordiaHelper.languageToDocumentLanguageCode(account.properto__Country_Of_Origin__c);
        customerDtoCordia.street = getAddressFromOrganisation(account);
        customerDtoCordia.zipCode = account.BillingPostalCode;
        customerDtoCordia.countryRegionId = countryToIsoCode(account.properto__Country_Of_Origin__c);
        customerDtoCordia.city = account.BillingCity;
        customerDtoCordia.defaultCurrency = account.CurrencyIsoCode;
        customerDtoCordia.customerGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDtoCordia.defaultCurrency = account.CurrencyIsoCode;
        customerDtoCordia.vatNum = getVatNum(account);
        customerDtoCordia.taxGroup = INT_CordiaHelper.getTaxGroupByCountryOfOrigin(account.properto__Country_Of_Origin__c);
        customerDtoCordia.customerBankAccount = account.properto__Bank_Acc_No__c;
        customerDtoCordia.customerBankGroup = account.properto__Bank_Acc_Name__c;
        customerDtocordia.customerEscrowBankAccount = salesProcessToSend.properto__Main_Resource__r.properto__Bank_Account_Number__c;
        customerDtocordia.customerEscrowBankGroup = salesProcessToSend.properto__Main_Resource__r.properto__Bank_Name__c;
        return customerDtoCordia;
    }

    public static List<INT_CustomerDto_Cordia> accountsListToCustomerList(List<Account> accounts) {
        List<INT_CustomerDto_Cordia> customerDto_cordias = new List<INT_CustomerDto_Cordia>();
        for (Account account : accounts) {
            customerDto_cordias.add(accountToCustomer(account));
        }
        return customerDto_cordias;
    }

    public enum PartyType {
        NONE, PERSON, ORGANISATION
    }

    private static String languageToDocumentLanguageCode(String language) {
        String tempLanguage;
        if (language != null) {
            tempLanguage = language;
            tempLanguage.toLowerCase();
        } else {
            tempLanguage = 'default';
        }
        if (tempLanguage == 'poland') {
            return 'pl';
        } else if (tempLanguage == 'hungary') {
            return 'hu';
        } else if (tempLanguage == 'romania') {
            return 'en-za';
        } else {
            return 'en';
        }
    }


    @TestVisible
    public static String countryToIsoCode(String countryName) {
        if (isCorrectCountryIsoCode(countryName)) {
            return countryName;
        } else if (String.isEmpty(countryName)) {
            throw new INT_ValidationException('Country cannot be empty');
        } else {
            String tempCountryName = countryName.toLowerCase();
            Map<String, String> countryCodeMap = getCountryTranslationMap();
            return countryCodeMap.get(tempCountryName);

        }


    }

    @TestVisible
    private static String getVatNum(Account account) {
        if (!isEmptyOrNull(account.properto__PESEL__c) || !isEmptyOrNull(account.properto__NIP__c)) {
            if (!isEmptyOrNull(account.properto__NIP__c)) {
                return account.properto__NIP__c;
            } else if (!isEmptyOrNull(account.properto__PESEL__c)) {
                return account.properto__PESEL__c;
            }

        } else {
            throw new INT_ValidationException('VATNum cannot be empty');
        }
        return null;
    }

    @TestVisible
    private static Boolean isCorrectCountryIsoCode(String code) {
        Set<String> allowedIsoCodeSet = new Set<String>();
        allowedIsoCodeSet.add('HUN');
        allowedIsoCodeSet.add('POL');
        allowedIsoCodeSet.add('ROU');
        if (allowedIsoCodeSet.contains(code)) {
            return true;
        } else {
            return false;
        }
    }

    @TestVisible
    private static Map<String, String> getCountryTranslationMap() {
        Map<String, String> countryCodeMap = new Map<String, String>();
        countryCodeMap.put('poland', 'POL');
        countryCodeMap.put('polska', 'POL');
        countryCodeMap.put('romania', 'ROU');
        countryCodeMap.put('rumunia', 'ROU');
        countryCodeMap.put('hungary', 'HUN');
        countryCodeMap.put('węgry', 'HUN');
        countryCodeMap.put('wegry', 'HUN');
        return countryCodeMap;
    }

    @TestVisible
    private static Boolean isEmptyOrNull(String value) {
        if (value == null || String.isEmpty(value)) {
            return true;
        } else {
            return false;
        }
    }

    private static String getAddress(Account account) {
        String address;
        if (account.RecordType.DeveloperName == 'Individual_Client') {
            address = getAddressFromIndividualClient(account);
        } else {
            address = getAddressFromOrganisation(account);
        }
        return address;

    }
    private static String getAddressFromIndividualClient(Account account) {

        String address;
        if (account.properto__Building_No__pc != null) {
            if (account.properto__Unit_No__pc == null) {
                address = account.BillingStreet + ' ' + account.properto__Building_No__pc;
            } else {
                address = account.BillingStreet + ' ' + account.properto__Building_No__pc + '/' + account.properto__Unit_No__pc;
            }
        } else {
            address = getAddressFromOrganisation(account);
        }

        return address;
    }

    private static String getAddressFromOrganisation(Account account) {
        String address;
        if (isEmptyOrNull(account.properto__Unit_No__c)) {
            address = account.BillingStreet + ' ' + account.properto__Building_No__c;
        } else if (!isEmptyOrNull(account.properto__Unit_No__c)) {
            address = account.BillingStreet + ' ' + account.properto__Building_No__c + '/' + account.properto__Unit_No__c;
        }

        return address;
    }

    private static String bankCodeByAccountNumber(String bankAccountNumber) {
        Map<String, String> bankNamesCodes = new Map<String, String>();
        String bankCode = bankAccountNumber.substring(2, 6);
        bankNamesCodes.put('1040', 'ABN');
        bankNamesCodes.put('2350', 'BNP');
        bankNamesCodes.put('1080', 'CITI');//HU
        bankNamesCodes.put('2540', 'CITI');//PL

        return bankNamesCodes.get(bankCode);


    }
}