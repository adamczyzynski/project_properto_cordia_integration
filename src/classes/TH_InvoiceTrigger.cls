/**
 * Created by adam on 30/11/18.
 */

public without sharing class TH_InvoiceTrigger extends properto.TriggerHandler.DelegateBase {


    List<properto__Invoice__c> invoicesToSendCordia;


    public override void prepareBefore() {}

    public override void prepareAfter() {
        invoicesToSendCordia = new List<properto__Invoice__c>();
    }



    public override void afterInsert(Map<Id, sObject> o) {
        if (TriggerHelper.userProfile == true || TriggerHelper.switchOnTriggers(UserInfo.getUserId())) {
            Map<Id, properto__Invoice__c> spNew = (Map<Id, properto__Invoice__c>) o;

            for (Id keyNew : spNew.keySet()) {
                properto__Invoice__c spT = spNew.get(keyNew);

                if (spT.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_INVOICE,
                        properto.CommonUtility.INVOICE_TYPE_ADVANCEINVOICE) || spT.RecordTypeId == properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_INVOICE,
                        properto.CommonUtility.INVOICE_TYPE_FINAL_INVOICE)) {
                    invoicesToSendCordia.add(spT);
                }
            }

        }
        
    }


    public override void finish() {

        if(invoicesToSendCordia != null && !invoicesToSendCordia.isEmpty()) {
            System.enqueueJob(new INT_InvoiceQueueable_Cordia(invoicesToSendCordia));
        }

    }

}