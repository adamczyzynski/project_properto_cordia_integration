/**
 * Created by adam on 21/12/18.
 */

@IsTest
public class INT_CordiaHelperTest {

    @IsTest
    public static void shouldReturnCorrectTaxGroup() {
        System.assertEquals('CUS-POL', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('Poland'));
        System.assertEquals('CUS-POL', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('poland'));
        System.assertEquals('Hungarian domestic customer', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('hungary'));
        System.assertEquals('CUS-EU', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('bulgaria'));
        System.assertEquals('CUS-RO', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('romania'));
        System.assertEquals('CUS-EU', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('united kingdom'));
        System.assertEquals('CUS-NONEU', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('United States'));
        System.assertEquals('CUS-NONEU', INT_CordiaHelper.getTaxGroupByCountryOfOrigin('China'));

    }


    @IsTest
    public static void shouldReturnCorrectAXSalesProcessAgreementId() {
        String companyCode = 'PCW1';
        String salesProcessName = 'SP-1010';

        System.assertEquals('PCW1SP-1010', INT_CordiaHelper.salesProcessToCordiaAgreementId(salesProcessName, companyCode));
    }

    @IsTest
    public static void shouldReturnCorrectSalesforceIdBasedOnAxSalesProcessId() {
        String axSalesProcessId = 'PCW1SP-1010';

        Account parentAcount = INT_TestHelper.createParentAccount();
        Account childAccount = INT_TestHelper.createChildAccount(parentAcount);
        Contact contact = INT_TestHelper.createNewContactFromAccount(childAccount);

        properto__Sales_Process__c processTemplate = new properto__Sales_Process__c();
        processTemplate.properto__Contact__c = contact.Id;
        List<properto__Sales_Process__c> process = properto.TestHelper.createSalesProcessesProposal(processTemplate, 1, false);
        System.debug('wielkosc: ' + process.size());
        insert process;



/*        properto__Sales_Process__c process = new properto__Sales_Process__c();
        process.RecordTypeId = properto.CommonUtility.getRecordTypeId(properto.CommonUtility.SOBJECT_NAME_SALESPROCESS,
                properto.CommonUtility.SALES_PROCESS_TYPE_OFFER);
        process = properto.TestHelper.createSalesProcessProposal(null, null);
        insert process;*/
    }

}