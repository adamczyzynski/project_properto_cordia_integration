/**
 * Created by adam on 07/01/19.
 */

public without sharing class INT_TestHelper{


    public static Account createParentAccount() {

        Account parentAcc = new Account();
        parentAcc.RecordTypeId = properto.CommonUtility.getRecordTypeId('Account', 'Company');
        parentAcc.Name = 'Account_P-' + String.valueOf(Math.random());
        parentAcc.properto__NIP__c = String.valueOf(properto.TestHelper.createNIP());
        parentAcc.properto__Country_Of_Origin__c = 'Poland';
        insert parentAcc;
        return parentAcc;
    }

    public static Account createChildAccount(Account parentAccount) {
        Account accountTemplate = new Account();
        accountTemplate.properto__Country_Of_Origin__c = 'Poland';
        accountTemplate.ParentId = parentAccount.Id;
        Account childAccount = properto.TestHelper.createAccountStandard(accountTemplate, false);
        childAccount.properto__Country_Of_Origin__c = 'Poland';
        insert childAccount;

        return childAccount;
    }

    public static Contact createNewContactFromAccount(Account account) {
        Contact contractTemplate = new Contact();
        contractTemplate.AccountId = account.Id;

        Contact contact = properto.TestHelper.createContactPerson(contractTemplate, false);
        contact.properto__Country_Of_Origin__c = 'Poland';
        insert contact;
        return contact;
    }

}