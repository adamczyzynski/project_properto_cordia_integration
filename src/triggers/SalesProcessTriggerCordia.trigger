/**
 * Created by adam on 29/11/18.
 */

trigger SalesProcessTriggerCordia on properto__Sales_Process__c (
        before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    properto.TriggerHandler.execute(new TH_SalesProcessTrigger());
    System.debug('test from SP trigger');

}