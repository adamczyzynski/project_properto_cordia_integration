/**
 * Created by adam on 30/11/18.
 */

trigger InvoiceCordiaTrigger
        on properto__Invoice__c
        (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    properto.TriggerHandler.execute(new TH_InvoiceTrigger());
}